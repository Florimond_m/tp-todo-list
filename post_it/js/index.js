const newItemButton = document.getElementById('newItem');
const itemNameInput = document.getElementById('itemName');
const itemTextInput = document.getElementById('itemText');
const awaitingList = document.getElementById('awaiting');
const inProgressList = document.getElementById('inprogress');
const completedList = document.getElementById('completed');


function getMaxItemIdFromLocalStorage() {
  let maxId = 0;

  for (let i = 0; i < localStorage.length; i++) {
    const key = localStorage.key(i);
    if (key.startsWith('list_')) {
      const itemId = parseInt(key.substring(5));
      maxId = Math.max(maxId, itemId);
    }
  }

  return maxId;
}

let id = getMaxItemIdFromLocalStorage() + 1;

newItemButton.addEventListener('click', function (event) {
  if (!itemNameInput.checkValidity() || !itemTextInput.checkValidity()) {
    event.preventDefault();
    alert('Veuillez remplir tous les champs obligatoires.');
  } else {
    const itemName = itemNameInput.value;
    const itemText = itemTextInput.value;
    const itemKey = 'list_' + id;

    const newItem = createNewItem(itemName, itemText, getRandomColor(), itemKey);

    awaitingList.appendChild(newItem);

    itemNameInput.value = '';
    itemTextInput.value = '';

    const data = {
      title: itemName,
      content: itemText,
      color: newItem.style.backgroundColor
    };
    window.localStorage.setItem(itemKey, JSON.stringify(data));



    // Activer le glisser-déposer pour le nouvel élément
    enableDragAndDrop(newItem);
  }
});

function createNewItem(itemName, itemText, randomColor, itemKey) {
  const newItem = document.createElement('li');
  newItem.classList.add('post-it');
  newItem.id = itemKey.substring(5); // Supprime "list_" du début pour obtenir l'ID réel
  newItem.style.backgroundColor = randomColor;

  const contentContainer = document.createElement('div');
  contentContainer.classList.add('content-container');

  const itemTitle = document.createElement('h3');
  itemTitle.contentEditable = true; // Rend le titre éditable
  itemTitle.innerText = itemName;
  itemTitle.addEventListener('input', function () {
    updateItemTextInLocalStorage(itemKey, 'title', itemTitle.innerText);
  });

  const itemContent = document.createElement('p');
  itemContent.contentEditable = true; // Rend le contenu éditable
  itemContent.innerText = itemText;
  itemContent.addEventListener('input', function () {
    updateItemTextInLocalStorage(itemKey, 'content', itemContent.innerText);
  });

  const deleteImg = document.createElement('img');
  deleteImg.src = 'img/red_cross_sansfond.png';
  deleteImg.classList.add('delete-button');
  deleteImg.addEventListener('click', function () {
    newItem.remove();
    window.localStorage.removeItem(itemKey);
  });

  contentContainer.appendChild(itemTitle);
  contentContainer.appendChild(itemContent);
  contentContainer.appendChild(deleteImg);
  newItem.appendChild(contentContainer);

  return newItem;
}

function updateItemTextInLocalStorage(itemKey, property, value) {
  const data = JSON.parse(localStorage.getItem(itemKey));
  data[property] = value;
  window.localStorage.setItem(itemKey, JSON.stringify(data));
}



function enableDragAndDrop(item) {
  item.draggable = true;

  item.addEventListener('dragstart', function (event) {
    event.dataTransfer.setData('text/plain', item.id);
    event.dataTransfer.effectAllowed = 'move';
    item.classList.add('dragging');
  });

  item.addEventListener('dragend', function () {
    item.classList.remove('dragging');
  });

  item.addEventListener('dragover', function (event) {
    event.preventDefault();
    item.classList.add('dragover');
  });

  item.addEventListener('dragleave', function () {
    item.classList.remove('dragover');
  });
}


function drop(event, column) {
  event.preventDefault();
  const itemId = event.dataTransfer.getData('text/plain');
  const draggedItem = document.getElementById(itemId);

  if (column === 'awaiting') {
    const afterElement = getDragAfterItem(awaitingList, event.clientY);
    awaitingList.insertBefore(draggedItem, afterElement);
  } else if (column === 'inprogress') {
    const afterElement = getDragAfterItem(inProgressList, event.clientY);
    inProgressList.insertBefore(draggedItem, afterElement);
  } else if (column === 'completed') {
    const afterElement = getDragAfterItem(completedList, event.clientY);
    completedList.insertBefore(draggedItem, afterElement);
  }

  const itemKey = 'list_' + draggedItem.id;
  const data = JSON.parse(localStorage.getItem(itemKey));
  data.column = column;
  window.localStorage.setItem(itemKey, JSON.stringify(data));
}

function getDragAfterItem(container, y) {
  const draggableItems = Array.from(container.getElementsByClassName('post-it'));

  return draggableItems.reduce(function (closest, child) {
    const box = child.getBoundingClientRect();
    const offset = y - box.top - box.height / 2;

    if (offset < 0 && offset > closest.offset) {
      return { offset: offset, element: child };
    } else {
      return closest;
    }
  }, { offset: Number.NEGATIVE_INFINITY }).element;
}

function getRandomColor() {
  const colors = ['#ff4c4c', '#ff9f4c', '#ffcd4c', '#4cff4c', '#4cffff', '#4c9fff', '#4c4cff', '#9f4cff', '#ff4cff'];
  return colors[Math.floor(Math.random() * colors.length)];
}

function allowDrop(event) {
  event.preventDefault();
}

function loadItemsFromLocalStorage() {
  for (let i = 0; i < localStorage.length; i++) {
    const key = localStorage.key(i);
    if (key.startsWith('list_')) {
      const data = JSON.parse(localStorage.getItem(key));
      const newItem = createNewItem(data.title, data.content, data.color, key);
      if (data.column === 'awaiting') {
        awaitingList.appendChild(newItem);
      } else if (data.column === 'inprogress') {
        inProgressList.appendChild(newItem);
      } else if (data.column === 'completed') {
        completedList.appendChild(newItem);
      }
      enableDragAndDrop(newItem);
    }
  }
}

// Récupérer tous les éléments post-it existants du localStorage
const listItems = document.getElementsByClassName('post-it');

for (const item of listItems) {
  enableDragAndDrop(item);
}



loadItemsFromLocalStorage();
